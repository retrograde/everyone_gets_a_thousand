class HomeController < ApplicationController
  def index
  end

  def help
  end

  def leaderboard
    @points = Point.all.includes(:user).order(amount: :desc)
  end
end
