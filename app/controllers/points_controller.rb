class PointsController < ApplicationController
  def index
    @transactions = PointTransaction.where('sender_id=? OR reciever_id=?', current_user.id, current_user.id).order(created_at: :desc)
  end

  def deposit
    unless params[:amount].present? and  reciever = User.find(params[:reciever])
      flash[:warning] = "Failed to send points"
      redirect_to points_path
      return
    end

    if current_user == reciever
      flash[:danger] = "Try to give yourself points.... for shame...."
      redirect_to points_path
      return
    end

    amount = BigDecimal.new(params[:amount])

    if amount >= current_user.points.amount
      flash[:warning] = "You don't have enough points to send #{amount.to_s}"
      redirect_to points_path
      return
    elsif amount <= 0
      flash[:warning] = "There's no such thing as a negative point, and sending 0 points is frowned upon >:("
      redirect_to points_path
      return
    end

    ActiveRecord::Base.transaction do
      point_transaction = PointTransaction.new(sender: current_user, reciever: reciever, amount: amount, is_committed: true)
      current_user.points.remove_points!(amount)
      reciever.points.add_points!(amount)
      point_transaction.save!
    end

    flash[:success] = "You have deposited #{amount.to_s} points with #{reciever.email}.  Now you just have to get them back some day!"
    redirect_to points_path
  end
end
