class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :points, class_name: 'Point'
  validates_presence_of :points

  before_validation(on: :create) do
    if self.points.nil?
      self.points = Point.create(amount: 1000.00)
    end
  end

  has_many :sent_transactions, as: :sender, foreign_key: :sender_id
  has_many :recieved_transactions, as: :reciever, foreign_key: :reciever_id

  before_validation

protected
  # TODO remove this before live
  def confirmation_required?
    false
  end
end
