class Point < ApplicationRecord
  belongs_to :user

  validates :amount, numericality: { greater_than_or_equal_to: 0 }

  def add_points!(added_amount)
    if added_amount.is_a? Numeric
      self.amount += added_amount
      save!
    else
      # TODO throw error
    end
  end

  def remove_points!(removed_amount)
    if removed_amount.is_a? Numeric
      self.amount -= removed_amount
      save!
    else
      # TODO throw error
    end
  end
end
