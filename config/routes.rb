Rails.application.routes.draw do
  resources :points, only: [:index] do
    post :deposit, on: :collection
  end

  devise_for :users

  resources :home, only: [:index] do
    get :leaderboard, on: :collection
    get :help, on: :collection
  end

  root to: "home#index"

end
