class CreatePointTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :point_transactions do |t|
      t.integer :sender_id, null: false, index: :true
      t.integer :reciever_id, null: false, index: :true
      t.decimal :amount
      t.string :reason
      t.boolean :is_committed, default: false, null: false

      t.timestamps
    end

    add_index :point_transactions, [:sender_id, :reciever_id]
  end
end
